package logic;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CreateXlsx {

	/**
	 * 用于缓存单元格格式.
	 */
	private static final Map<Integer, XSSFCellStyle> CACHE = new HashMap<>(64000);
	public  static List<int[]> list = new ArrayList<int[]>();
	public static XSSFCellStyle xstyle;
	public CreateXlsx(List<int[]> list){
		this.list = list;
	}
	/**
	 * 生成成品Excel.
	 *
	 * @param path
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public void create( String path) throws Exception {

		List<int[]> list = this.list;
		int size = list.size();
		int length = list.get(0).length;

		// 先保存一个空Excel文件，在磁盘占位
		SXSSFWorkbook excel = new SXSSFWorkbook(size);
		excel.createSheet("myPic");
		xstyle = (XSSFCellStyle) excel.createCellStyle();
		FileOutputStream fos = new FileOutputStream(path);
//		path = saveExcel(path, excel);


		/**
		 * 使用线程池进行线程管理。
		 */
		ExecutorService es = Executors.newCachedThreadPool();
		/**
		 * 使用计数栅栏
		 */
		CountDownLatch doneSignal = new CountDownLatch(size);

		// 读取占位的文件，设置列宽
		System.out.println("Start setting the style of cells…");
		Sheet sht = excel.getSheetAt(0);
		for (int j = 0; j < length; j++)
			sht.setColumnWidth(j, (short) 500);

		// 缓存所需的所有单元格格式

		for (int k = 0; k < size; k++){
			sht.createRow(k);
		}

		for (int i = 0; i < size; i++){

			es.submit(new PoiWriter(doneSignal, sht, i));
			Thread.sleep(100);
//
		}
		doneSignal.await();
		es.shutdown();

		excel.write(fos);
		System.out.println("Done");// 提示完成
	}

	private static String saveExcel(final String path, XSSFWorkbook excel) throws Exception {
		try {
			FileOutputStream out = new FileOutputStream(path);
			excel.write(out);
			excel.close();
			out.close();
		} catch (Exception e) {
			throw new Exception("Cannot write an excel file to disk.", e);
		}
		return path;
	}
	protected static class PoiWriter implements Runnable {

		private final CountDownLatch doneSignal;
		private Sheet sheet;
		private int line;
		public PoiWriter(CountDownLatch doneSignal, Sheet sheet,int line
						 ) {
			this.doneSignal = doneSignal;
			this.sheet = sheet;
			this.line = line;
		}

		public void run() {
			int i = line;
			try {
				int[] rgbs = list.get(i);
				Row row = sheet.getRow(i);
				if(row == null) sheet.createRow(i);
				try{
					row.setHeight((short) 250);
				}catch (NullPointerException e) {
					System.out.println("error:"+i);
				}

				for (int rgb : rgbs) {
					Integer key = Integer.valueOf(rgb);
					if (CACHE.get(key) == null) {
						XSSFCellStyle style = (XSSFCellStyle) xstyle.clone();
						style.setFillForegroundColor(new XSSFColor(new Color(rgb)));
						style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
						CACHE.put(key, style);
					}
				}
				for (int j = 0; j < rgbs.length; j++)
					row.createCell(j).setCellStyle(CACHE.get(rgbs[j]));
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				doneSignal.countDown();
				System.out.println("line:" + i + " Count: " + doneSignal.getCount());
			}
		}

	}
}